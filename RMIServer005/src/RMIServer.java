import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIServer {
	static Registry r=null;
	static PlacesListInterface placeList=null;
		
	public static void main(String[] args) {
		
		try{
			r = LocateRegistry.createRegistry(Integer.parseInt(args[0]));
		}catch(RemoteException a){
			System.out.println("Tentei criar o registry, mas ele já existia!!!");
		}
		
		try{
		    placeList = new PlacesManager(Integer.parseInt(args[0]));	
			r.rebind("placelist", placeList );

			System.out.println("Place server ready");
		}catch(Exception e) {
			System.out.println("Place server main " + e.getMessage());}
	}

}
