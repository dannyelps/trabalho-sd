import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class ObjectRegistry extends UnicastRemoteObject implements ObjectRegistryInterface{

	private static final long serialVersionUID = 1L;
	
	private HashMap<String, String> r = new HashMap<String,String>();
	
	public ObjectRegistry() throws RemoteException {
		super();
	}

	@Override
	public void addRManager(String objectID, String serverAddress) throws RemoteException {
		r.put(objectID, serverAddress);
	}

	@Override
	public String resolve(String objectID) throws RemoteException {
		return r.get(objectID);
	}
	
	
}
