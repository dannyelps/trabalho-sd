
public class Pessoa {

	private String primeiroNome;
	private String ultimoNome;
	
	public Pessoa(String primeiroNome, String ultimoNome) {
		super();
		this.primeiroNome = primeiroNome;
		this.ultimoNome = ultimoNome;
	}

	public synchronized String getPrimeiroNome() {
		return primeiroNome;
	}

	public synchronized void setNome(String primeiroNome, String ultimoNome) {
		this.primeiroNome = primeiroNome;
		this.ultimoNome = ultimoNome;
	}
		
	public synchronized void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	public synchronized String getUltimoNome() {
		return ultimoNome;
	}

	public synchronized void setUltimoNome(String ultimoNome) {
		this.ultimoNome = ultimoNome;
	}
	
	
}
