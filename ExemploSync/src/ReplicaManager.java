import java.util.ArrayList;

public class ReplicaManager {
	ArrayList<String> replicas = new ArrayList<String>();
	
	public synchronized void addReplica(String rep) {
		this.replicas.add(rep);
	}
	
	public synchronized void mostraReplicas() {
		for(String s: this.replicas)
			System.out.println(s);
		
		System.out.println("===============");
	}
	
}
